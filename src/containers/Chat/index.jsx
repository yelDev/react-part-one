import React, { useEffect, useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, LinearProgress, Paper } from "@material-ui/core";
import moment from "moment";


import ChatHeader from "../../components/ChatHeader";
import Message from "../../components/Message";

import reducer, { initialState } from "./reducer";
import * as constants from "./constants";

import { v4 } from "uuid";
import CreateMessage from "../../components/CreateMessage";

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(2)
  },
}));

const Chat = (props) => {
  const classes = useStyles();
  const { url } = props;
  const [state, dispatch] = useReducer(reducer, initialState);
  const { isLoading, messages, users, recentDate, currentUser, isSending } = state;

  const [edit, setEdit] = React.useState(null);

  const openEdit = (id) => {
    setEdit(id);
  }

  const closeEdit = () => {
    setEdit(null);
  }

  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .then(data => getMessageData(data))
      .catch(error => dispatch({ type: constants.GET_MESSAGES_FAIL }))
      .finally(() => console.log("final"));
  }, []);

  React.useMemo(() => {
    if (!messages) {
      return;
    }
    const messagesLastItem = messages.length - 1; 
    const lastDate = moment(messages[messagesLastItem].createdAt).format();
    dispatch({ type: constants.UPDATE_LAST_MESSAGE_RECEIVED, payload: lastDate })
  }, [messages]);

  const getMessageData = (data) => {
    // get users from messages
    const getUsers = data.filter((obj, pos, arr) => arr.map(mapObj => mapObj["id"]).indexOf(obj["id"]) === pos)
      .map(item => ({
        id: item.id,
        avatar: item.avatar,
        createdAt: item.createdAt,
        editedAt: item.editedAt,
        user: item.user
      }));

    const sortData = data.sort(function(a,b){
      return new Date(b.date) - new Date(a.date);
    });

    dispatch({ type: constants.GET_MESSAGES_SUCCESS, payload: {
      messages: sortData.map(item => ({...item, messageId: v4(), likes: []})),
      users: getUsers
    }})
  }

  const sendMessage = message => {
    dispatch({ type: constants.CREATE_MESSAGE_REQUEST });
    const { user, id, avatar } = currentUser;
    const createdAt = moment().format();
    const editedAt = "";
    const generateMessage = {
      user,
      id,
      avatar,
      createdAt,
      editedAt,
      text: message,
      likes: [],
      messageId: v4()
    }
    dispatch({ type: constants.CREATE_MESSAGE_SUCCESS, payload: generateMessage });
  }
  
  const deleteMessage = ({ id, messageId }) => e => {
    if (id !== currentUser.id) {
      return;
    }
    dispatch({ type: constants.DELETE_MESSAGE_SUCCESS, payload: messageId });
  }

  const updateMessage = (message) => {
    if (!edit) {
      return;
    }
    const messageToUpdate = messages.find(el => el.messageId === edit);
    const newMessage = {...messageToUpdate, text: message, editedAt: moment().format()};
    dispatch({ type: constants.EDIT_MESSAGE_SUCCESS, payload: newMessage })
    closeEdit();
  }

  const reaction = (item) => e => {
    const { id, likes } = item;
    if (id === currentUser.id) {
      return;
    }
    let result;

    const check = likes.findIndex(item => item.id === currentUser.id);
    const addLike = (message, user) => {
      const { likes } = message;
      const newLikes = [...likes, user];
      return {...message, likes: newLikes}
    };

    const removeLike = (message, userId) => {
      const { likes } = message;
      const newLikes = likes.filter(item => item.id !== userId);
      return {...message, likes: newLikes}
    }

    check === -1 
      ? result = addLike(item, currentUser)
      : result = removeLike(item, currentUser.id)

    dispatch({ type: constants.ADD_LIKE, payload: result });
  }

  return (
    <>
      {isLoading ? <LinearProgress/> : (
        <Paper className={classes.root}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <ChatHeader count={messages.length} participants={users} recentDate={recentDate}/>
            </Grid>
            <Grid item xs={12}>
              {messages.map(item => (
                <Message
                  key={v4()}
                  position={item.id === currentUser.id ? "end" : "start"}
                  item={item}
                  deleteMessage={deleteMessage}
                  currentUserId={currentUser.id}
                  reaction={reaction}
                  openEdit={openEdit}
                  closeEdit={closeEdit}
                  edit={edit}
                  updateMessage={updateMessage}
                />
              ))}
            </Grid>
            <Grid item xs={12}>
              <CreateMessage createMessage={sendMessage}/>
              {isSending && <LinearProgress/>}
            </Grid>
          </Grid>
        </Paper>
      )}
    </>
  )
}

export default Chat;
