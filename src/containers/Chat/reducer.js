import * as constants from "./constants";

export const initialState = {
  isLoading: true,
  messages: null,
  currentUser: 
    {
      avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
      id: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
      user: "Ben",
    },
  users: [
    {
      avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      createdAt: "2020-07-16T19:48:12.936Z",
      editedAt: "",
      id: "9e243930-83c9-11e9-8e0c-8f55286f4ce4",
      user: "Elon",
    },
    {
      avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      createdAt: "2020-07-16T19:48:12.936Z",
      editedAt: "",
      id: "9e243930-83c9-11e9-8e0c-8f5528856ce4",
      user: "Mask",
    },
    {
      avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      createdAt: "2020-07-16T19:48:12.936Z",
      editedAt: "",
      id: "9e243930-83c9-11e9-8e0c-8f55286412e4",
      user: "John",
    },
  ],
  recentDate: null,
  error: null,
  isSending: false
}

export default (state=initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case constants.GET_MESSAGES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        messages: payload.messages,
        users: [...state.users, ...payload.users],
        recentDate: payload.recentDate
      }
    case constants.GET_MESSAGES_FAIL:
      return {
        ...state,
        error: "Error"
      }
    case constants.CREATE_MESSAGE_REQUEST:
      return {
        ...state,
        isSending: true
      }
    case constants.CREATE_MESSAGE_SUCCESS:
      return {
        ...state,
        isSending: false,
        messages: [...state.messages, payload]
      }
    case constants.DELETE_MESSAGE_SUCCESS:
      return {
        ...state,
        messages: state.messages.filter(item => item.messageId !== payload)
      }
    case constants.ADD_LIKE:
      return {
        ...state,
        messages: state.messages.map(item => item.messageId !== payload.messageId ? item : payload )
      }
    case constants.EDIT_MESSAGE_SUCCESS:
      return {
        ...state,
        messages: state.messages.map(item => item.messageId !== payload.messageId ? item : payload )
      }
    case constants.UPDATE_LAST_MESSAGE_RECEIVED:
      return {
        ...state,
        recentDate: payload
      }
    default:
      return state;
  }
}