import React from 'react';
import { makeStyles, useTheme, withTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Header from "../../components/AppHeader";
import Footer from "../../components/Footer";
import { CssBaseline, Container } from '@material-ui/core';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    paddingTop: 15,
    height: "84%",
    overflow: "auto"
  },
}));

const Layout = (props) => {
  const classes = useStyles();
  return (
    <Container style={{ height: "100vh" }}>
      <CssBaseline/>
      <Header />
      <main className={classes.content}>
        {props.children}
      </main>
      <Footer/>
    </Container>
  )
}

export default Layout;