import React from 'react';

import Layout from "./containers/Layout";
import Chat from "./containers/Chat";

function App() {
  const url = "https://api.npoint.io/a139a0497ad54efd301f";
  return (
    <Layout>
      <Chat url={url}/>
    </Layout>
  );
}

export default App;
