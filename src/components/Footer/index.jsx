import React from 'react'
import { Typography } from '@material-ui/core'

const Footer = () => {
  return (
    <Typography align="center" variant="subtitle1" style={{ heigth: "8%" }}>Copyright</Typography>
  )
}

export default Footer
