import React from 'react'
import { makeStyles, withStyles } from "@material-ui/core/styles";
import moment from "moment";

import RedditIcon from '@material-ui/icons/Reddit';
import {AppBar, Toolbar, Typography, IconButton, Grid} from "@material-ui/core";

const StyledAppBar = withStyles({
  root: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    "& > div": {
      minHeight: 48,
    }
  },
})(AppBar);

const useStyles = makeStyles(theme => ({
  headerInfo: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    justifyContent: "space-evenly"
  }
}));

const ChatHeader = ({ count, participants, recentDate }) => {
  const classes = useStyles();
  return (
      <StyledAppBar
      position="static"
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
        >
          <RedditIcon />
          <Typography variant="h6">
            Chat 1
          </Typography>
        </IconButton>
        <article className={classes.headerInfo}>
          <Typography variant="h6">
            Participants
            {participants.length}
          </Typography>
          <Typography variant="h6">
            Messages
            {count}
          </Typography>
          <Typography variant="caption">
            Last message
            {moment(recentDate).format("HH:mm")}
          </Typography>
        </article>
      </Toolbar>
    </StyledAppBar>
  )
}

export default ChatHeader;
