import React from 'react';
import { makeStyles } from "@material-ui/core";
import { Paper, Card, Divider, CardContent, CardActions,Avatar } from "@material-ui/core";
import moment from "moment";

import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import EditIcon from '@material-ui/icons/Edit';
import CreateMessage from '../CreateMessage';

const useStyles = makeStyles(theme => ({
  action: {
    padding: 4,
    display: "flex",
    justifyContent: "flex-end"
  },
  content: {
    display: "flex",
    alignItems: "center",
    padding: 8
  },
  like: {
    display: "flex",
    alignItems: "center",
  }
}));

const Messages = ({ position, item, deleteMessage, currentUserId, reaction, openEdit, edit, updateMessage }) => {
  const classes = useStyles();
  const isLiked = item.likes.find(el => el.id === currentUserId);

  return (
    <Paper style={{ display: "flex", justifyContent: `flex-${position}` }}>
      <Card style={{margin: "10px", maxWidth: "70%"}}>
      <CardContent className={classes.content}>
      {item.id !== currentUserId && <Avatar src={`${item.avatar}`} aria-label="recipe"/>}
        {(!edit || edit !== item.messageId) && (
          <Typography variant="h5" component="p" style={{padding: 8}}>
            {item.text}
          </Typography>
        )} 
        {edit && edit === item.messageId && <CreateMessage text={item.text} createMessage={updateMessage} />}
      </CardContent>
      <Divider/>
      <CardActions className={classes.action}>
        <div className={classes.like}>
          <Typography  variant="subtitle1">{item.likes.length}</Typography>
          <IconButton aria-label="add to favorites" onClick={reaction(item)} style={{ padding: 4 }} color={isLiked ? "secondary": "default"}>
            <FavoriteIcon size="small" />
          </IconButton>
        </div>
        {currentUserId === item.id && (
          <>
            <IconButton aria-label="add to favorites" style={{ padding: 4 }} onClick={deleteMessage(item)}>
              <HighlightOffIcon size="small"/>
            </IconButton>
            <IconButton aria-label="add to favorites" style={{ padding: 4 }} onClick={() => openEdit(item.messageId)}>
              <EditIcon size="small"/>
            </IconButton>
          </>
        )}
      </CardActions>
      </Card>
    </Paper>
  )
}

export default Messages
