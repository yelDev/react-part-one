import React, { useState } from 'react';
import { makeStyles} from "@material-ui/core/styles";
import { Button, TextField } from "@material-ui/core";
import SendIcon from '@material-ui/icons/Send';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  form: {
    display: "flex",
    padding: 8
  }
}));

const CreateMessage = ({ createMessage, text="" }) => {
  const classes = useStyles();
  const [message, setMessage] = useState(text);

  const messageHandler = (e) => {
    setMessage(e.target.value);
  }

  const onSubmit = e => {
    e.preventDefault();
    createMessage(message);
    setMessage("");
  }

  return (
    <form onSubmit={onSubmit} className={classes.form}>
      <TextField
        id="outlined-multiline-flexible"
        label="Write message"
        multiline
        fullWidth
        value={message}
        onChange={messageHandler}
        variant="outlined"
      />
      <Button
        variant="contained"
        color="primary"
        type="submit"
        disabled={!message ? true : false}
        className={classes.button}
        endIcon={<SendIcon />}
      >
        Send
      </Button>
    </form>
  )
}

export default CreateMessage
